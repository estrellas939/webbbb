+++
title = "Natural Language Processing 👋"
description = "final project for ids703."
date = 2024-01-30T07:00:00+00:00
updated = 2024-01-30T07:00:00+00:00
template = "test/page.html"
draft = false

[taxonomies]
authors = ["George Wang"]

[extra]
lead = '''
This is the final project for Duke IDS703 Natural Language Processing. Basically, it's a classifier that do with IMDB movie reviews and putting tags. That's it!!
'''
images = []
+++

Click [HERE](https://www.kaggle.com/datasets/lakshmi25npathi/imdb-dataset-of-50k-movie-reviews/data) to view the dataset.

Click [HERE](https://github.com/Estrellas-857/IDS703NLP_Final_Project/blob/main/IDS703_Final_project.pdf) to view the final report.

1. Data Pre-Processing

2. Naive-Bayes Model

3. Neural Network Model

4. Conclusion