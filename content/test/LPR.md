+++
title = "License Plate Recognize"
description = "Final project for Duke ECE 588 Image and Video Processing. I did all the work by myself...R.I.P"
date = 2024-01-30T09:19:42+00:00
updated = 2024-01-30T09:19:42+00:00
draft = false
template = "test/page.html"

[taxonomies]
authors = ["George Wang"]

[extra]
lead = "This is the Final project for Duke ECE 581 Image and Video Processing <b>ALL ON MY OWN</b>."
+++

<div class="article">
	<h2>0. First, let's have a overlook for the whole project:<h2>


<h3>License Plate Recognition (LPR) plays a pivotal role in applications ranging from traffic mon- itoring to law enforcement and automated toll collection. This study conducts a comparative analysis of LPR, contrasting traditional image processing techniques with advanced deep learning approaches. 

Starting with conventional Tesseract optical character recognition (OCR) methods, the research covers image pre-processing, segmentation, character recognition, and template match- ing, emphasizing their interpretability and control. The exploration then extends to modern OCR methods, including Tesseract OCR’s Long Short-Term Memory (LSTM) based forms, highlighting LSTM’s improved handling of complex text layouts. 

Subsequently, a Convolutional Neural Net- works (CNN) based model and an End-to-End CNN model for LPR are developed. Intriguingly, these models, while proficient in feature extraction and pattern recognition, does not surpass tra- ditional methods in speed and accuracy in our tests. This outcome underlines the complexities of applying deep learning to specific real-world scenarios. The comparative study evaluates these approaches, revealing that while deep learning approaches offer robustness and adaptability, tradi- tional methods maintain relevance through their precision and interpretability in certain contexts.

This research aims to inform the development of LPR systems, balancing traditional and deep learning techniques to meet the diverse needs of intelligent transportation systems.<h3>

<hr>

<h2>1. Traditional License Plate Recognize(LPR)<h2>

<h3>Traditional LPR is a way that using computer vision and OCR (Optical Character Recognition). 

Here's a breakdown of its approach:

1. Loading and Preprocessing: The script starts by loading an image using OpenCV (cv2.imread). It checks if the image is loaded correctly and then converts it to grayscale for further processing.

2. Edge Detection using Sobel Operator: It applies the Sobel operator, a popular edge detection technique, in both horizontal (sobel_x) and vertical (sobel_y) directions. These operations help in highlighting the edges in the image, which is useful for identifying the rectangular shape of a license plate.

3. Thresholding and Noise Reduction: The script uses Gaussian blur for noise reduction, followed by Otsu's thresholding method (cv2.threshold). Thresholding converts the image to a binary image, making it easier to extract features and contours.

4. Morphological Operations: It performs dilation and erosion, which are morphological operations to enhance the structure of the image. These steps help in connecting broken parts of the object (like characters on the license plate) and removing small, irrelevant details.

5. Contour Detection and License Plate Extraction: The script then uses cv2.findContours to find different contours in the image. It looks for a contour with an aspect ratio that matches a typical license plate and extracts that region from the image.

6. License Plate Recognition using Tesseract OCR: The extracted license plate region is then passed to Tesseract OCR (pytesseract.image_to_string), an open-source OCR engine, to recognize the characters on the license plate. The script specifies a whitelist of characters (numbers and uppercase letters) to improve accuracy.

7. Optional Post-processing: The code includes commented sections that indicate the potential for post-processing, such as displaying intermediate results (using matplotlib.pyplot) and refining OCR results with regular expressions.<h3>
</div>

```python
import cv2
import numpy as np
import matplotlib.pyplot as plt
import pytesseract
from PIL import Image
# Specify Tesseract-OCR file path (windows OS only)
pytesseract.pytesseract.tesseract_cmd = 'C:/Program Files/Tesseract-OCR/tesseract.exe'


def extract_license_plate(image_path):
    # read image
    image = cv2.imread(image_path)
    # check if the image can be read correctly
    if image is None:
        print("Error: Unable to load image at path:", image_path)
        return

    # if the image was loaded correctly, then do the following processing
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # show the grey value image
    #plt.figure(figsize=(10, 6))
    #plt.subplot(131), plt.imshow(gray, cmap='gray'), plt.title('Gray Image')

    # Horizontal and vertical edge detection using Sobel operator
    sobel_x = cv2.Sobel(gray, cv2.CV_8U, 1, 0, ksize=3)  # horizonal
    sobel_y = cv2.Sobel(gray, cv2.CV_8U, 0, 1, ksize=3)  # vertical

    # Combine horizontal and vertical edge detection results
    sobel_combined = cv2.bitwise_or(sobel_x, sobel_y)

    # Display Sobel edge detection results
    #plt.subplot(132), plt.imshow(sobel_combined, cmap='gray'), plt.title('Sobel Edge Detection')

    blur = cv2.GaussianBlur(gray, (5, 5), 0)
    ret, threshold = cv2.threshold(blur, 0, 255, cv2.THRESH_OTSU + cv2.THRESH_BINARY)

    # Binarization
    #ret, threshold = cv2.threshold(sobel_combined, 0, 255, cv2.THRESH_OTSU + cv2.THRESH_BINARY)

    # show the binarization result
    #plt.subplot(133), plt.imshow(threshold, cmap='gray'), plt.title('Thresholding Result')
    #plt.show()

    # Erode and dilate images
    element1 = cv2.getStructuringElement(cv2.MORPH_RECT, (10, 6))
    element2 = cv2.getStructuringElement(cv2.MORPH_RECT, (6, 2))

    dilation = cv2.dilate(threshold, element2, iterations=1)
    erosion = cv2.erode(dilation, element1, iterations=1)
    #dilation2 = cv2.dilate(erosion, element2, iterations=3)

    # show the result after dilation and erosion
    #plt.figure(figsize=(10, 6))
    #plt.subplot(131), plt.imshow(dilation, cmap='gray'), plt.title('Dilation Result')
    #plt.subplot(132), plt.imshow(erosion, cmap='gray'), plt.title('Erosion Result')
    #plt.subplot(133), plt.imshow(dilation2, cmap='gray'), plt.title('Final Dilation Result')
    #plt.show()

    # looking up the plate region
    license_plate = None
    contours, hierarchy = cv2.findContours(erosion, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    for contour in contours:
        x, y, w, h = cv2.boundingRect(contour)
        aspect_ratio = float(w) / h
        if 2 < aspect_ratio < 5.5:  # Normally the aspect ratio of a license plate is between 2 and 5.5
            license_plate = image[y:y + h, x:x + w]
            cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 2)
            break  # Assume there is only one license plate, exit the loop after finding it

    # show results
    #plt.figure(figsize=(10, 6))
    #plt.subplot(121), plt.imshow(cv2.cvtColor(image, cv2.COLOR_BGR2RGB)), plt.title('Original Image')
    #if license_plate is not None:
        #plt.subplot(122), plt.imshow(cv2.cvtColor(license_plate, cv2.COLOR_BGR2RGB)), plt.title('License Plate')
    #else:
        #plt.subplot(122), plt.imshow(np.zeros((50, 150, 3), dtype=np.uint8)), plt.title('License Plate Not Found')
    #plt.show()

    return license_plate

def read_license_plate(license_plate_img):
    if license_plate_img is not None:
        #Convert OpenCV image to PIL image format
        license_plate_img_pil = Image.fromarray(cv2.cvtColor(license_plate_img, cv2.COLOR_BGR2RGB))

        #Tesseract configuration: use whitelist to limit character set
        #config = '-c tessedit_char_whitelist=0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ --psm 8'
        config = '--oem 0'
        # OCR using Tesseract
        text = pytesseract.image_to_string(license_plate_img_pil, config=config)

        # Use regular expressions for post-processing
        # import re
        # pattern = re.compile("[A-Z0-9]+")
        # matches = pattern.findall(text)
        # if matches:
        #     text = " ".join(matches)

        #print(text)
        return text
    else:
        print("No license plate found")




if __name__ == "__main__":
    image_path = 'D:/588way2/archive/test/1/80-ZL-029.png'#for test
    license_plate_img = extract_license_plate(image_path)
    read_license_plate(license_plate_img)
```

<div class = "article"> 
`println!` is a macro that prints text to the console.

A binary can be generated using the Rust compiler: `rustc`.
</div>

```python
import os
import time
from traditional_LPR import extract_license_plate, read_license_plate

# path setting
image_folder = 'D:/588way2/archive/test'  # Replace with picture folder path
max_images_to_process = 1000  # Set the maximum number of images to process

# statistical variables
total_images = 0
correctly_identified = 0
error_cases = []  # Used to record recognition errors

# Recording time starts
start_time = time.time()

# Get all image file names and limit the number of processes
image_files = [img for img in os.listdir(image_folder) if img.endswith(('.png', '.jpg', '.jpeg'))]
image_files = image_files[:max_images_to_process]  # Limit the number of processes

# Iterate through all image files in the folder
for image_name in image_files:
    total_images += 1
    image_path = os.path.join(image_folder, image_name)

    # Locate and identify license plates
    license_plate_image = extract_license_plate(image_path)
    identified_text = read_license_plate(license_plate_image)

    # Extract correct answer from file name
    correct_answer = image_name.split('.')[0].replace('-', '')

    # Compare results and handle possible errors
    if identified_text:
        identified_text_cleaned = identified_text.strip().replace(" ", "").upper()
        if identified_text_cleaned == correct_answer.upper():
            correctly_identified += 1
        else:
            error_cases.append((correct_answer, identified_text_cleaned))
    else:
        error_cases.append((correct_answer, 'None'))

# Print error cases, one line per case
for case in error_cases:
    print(f'Correct: {case[0]}, Identified: {case[1]}')

# Calculate running time
end_time = time.time()
total_time = end_time - start_time

# Calculate accuracy
accuracy = (correctly_identified / total_images) * 100

# Output statistical results
#print(error_cases)
print(f'Total images processed: {total_images}')
print(f'Correctly identified: {correctly_identified}')
print(f'Accuracy: {accuracy:.2f}%')
print(f'Total runtime: {total_time:.2f} seconds')
```

`rustc` will produce a `hello` binary that can be executed.

```bash
$ ./hello
Hello World!
```