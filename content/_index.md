+++
title = "Welcome!!"


# The homepage contents
[extra]
lead = '<b>George Wang</b> is an international student at Duke University majoring Electrical and Computer Engineering(ECE). <br>Click <a href="https://github.com/Estrellas-857">HERE</a> for his github.'
url = "/test/"
url_button = "View Portfolio"
repo_version = "GitHub v0.1.0"
repo_license = "Open-source MIT License."
repo_url = "https://github.com/aaranxu/adidoks"

# Menu items
[[extra.menu.main]]
name = "Docs"
section = "docs"
url = "/docs/getting-started/introduction/"
weight = 10

[[extra.menu.main]]
name = "Blog"
section = "blog"
url = "/blog/"
weight = 20

[[extra.menu.main]]
name = "Portfolio"
section = "test"
url = "/test/"
weight = 20

[[extra.list]]
title = "⚡️MENG @ Duke⚡️"
content = 'Get A+ scores on <a href="https://observatory.mozilla.org/analyze/adidoks.org">Mozilla Observatory</a> out of the box. Easily change the default Security Headers to suit your needs.'

[[extra.list]]
title = "⚡️BS @ NCAT ⚡️"
content = 'Get 100 scores on <a href="https://googlechrome.github.io/lighthouse/viewer/?gist=7731347bb8ce999eff7428a8e763b637">Google Lighthouse</a> by default. Doks removes unused css, prefetches links, and lazy loads images.'

[[extra.list]]
title = "⚡️BS @ HPU⚡️"
content = "Use sensible defaults for structured data, open graph, and Twitter cards. Or easily change the SEO settings to your liking."

[[extra.list]]
title = "⚡️Intern @ EPRI⚡️"
content = "Search your Doks site with FlexSearch. Easily customize index settings and search options to your liking."

[[extra.list]]
title = "⚡️Patent(CN116929758A)⚡️"
content = "Build pages with a landing page, blog, or documentation layout. Add custom sections and components to suit your needs."

[[extra.list]]
title = "⚡️Reserach @ John Deere⚡️"
content = "Switch to a low-light UI with the click of a button. Change colors with variables to match your branding."

+++
