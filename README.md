# Continuous Delivery of Personal Website

## Part I. Website Functionality

### 1. Build Website with Zola

I use AdiDoks as the theme.
- Each section has a `_index.md` file as the content for each page.
- `style.css` standardized some settings including text color, size, spacing and other configurations.

## Part II. Gitlab Workflow

### 2. Config File

`.gitlab-ci.yml` file is used to configure the CI/CD pipeline work of gitlab, which includes some settings about the method, stage, etc.

Now the Gitlab pipelines will monitor the commit of codes. Once a new commit is pushed, a new pipeline will start build and deploy automatically.

![screenshot of pipeline](https://gitlab.com/estrellas939/webbbb/-/raw/main/pic/pipeline.png?inline=false)

Static site link: https://webbbb-estrellas939-36aa29e7ec5632d021a8af576c9a534c11807ddae24.gitlab.io/

## Part III. Hosting and Deployment

### 3. AWS Amplify

Create a new app for hosting web app from Gitlab. And choosing the corresponding repository and branch.

- Edit `amplify.yml` as app build specification:
```
version: 1
frontend:
  phases:
    preBuild:
      commands:
        - wget https://github.com/getzola/zola/releases/download/v0.15.2/zola-v0.15.2-x86_64-unknown-linux-gnu.tar.gz
        - tar -xzvf zola-v0.15.2-x86_64-unknown-linux-gnu.tar.gz
    build:
      commands:
        - ./zola build
  artifacts:
    baseDirectory: public
    files:
      - '**/*'
  cache:
    paths: []
```

### 4. Test

AWS Amplify will also monitor the changes from repo. Once the repo has new changes, AWS Amplify will also do a new 'Provision - Build - Deploy' loop.

AWS Amplify hosting URL: https://main.d1kpj0tqdoojuy.amplifyapp.com/

![screenshot of app](https://gitlab.com/estrellas939/webbbb/-/raw/main/pic/amplify.png?inline=false)
